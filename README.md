# Words game
### Start the webserver:

* Via maven:

>sh install.sh (only tested on windows CMDer)

* Via docker:

>docker run toonlibbrecht/words:0.1.0

### Parse the file
Run the following command in the root folder to apply the words game on the input.txt file. 
Query parameter wordLength can be used to change the length of the "words".

>curl --request POST "http://localhost:8080/api/file" --data-binary @input.txt --header "Content-Type: text/plain" >> output.txt

Run the following command to apply the words game on an example input file.

>curl --request GET "http://localhost:8080/api/file/test" >> output.txt
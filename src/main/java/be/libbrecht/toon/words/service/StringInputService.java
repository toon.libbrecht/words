package be.libbrecht.toon.words.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class StringInputService implements InputService {

	@Override
	public Set<String> parseLines(String input) {
		return new HashSet<String>(Arrays.asList(input.split("\n")));
	}

}

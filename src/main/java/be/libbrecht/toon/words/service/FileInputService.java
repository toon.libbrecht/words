package be.libbrecht.toon.words.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import be.libbrecht.toon.words.exception.WordsException;

@Service
public class FileInputService implements InputService {

	@Override
	public Set<String> parseLines(String input) {
		try (InputStream stream = new ClassPathResource("static/" + input).getInputStream()) {
			return new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8)).lines().collect(Collectors.toSet());
		} catch (IOException e) {
			throw new WordsException(e);
		}
	}

}

package be.libbrecht.toon.words.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import be.libbrecht.toon.words.domain.Combo;
import be.libbrecht.toon.words.domain.WordPool;

@Service
public class WordsServiceImpl implements WordsService {

	@Override
	public WordPool createWordPool(Set<String> input, int resultWordLength) {
		Set<String> resultWords = new HashSet<>();
		Set<String> partialWord = new HashSet<>();

		for (String word : input) {
			if (word.length() == resultWordLength) {
				resultWords.add(word);
			} else if (word.length() > 0 && word.length() < resultWordLength) {
				partialWord.add(word);
			}
		}
		return new WordPool(resultWords, partialWord, resultWordLength);
	}

	@Override
	public List<Combo> findCombinations(WordPool pool) {
		Set<String> results = pool.getResults();
		List<Combo> combinations = new ArrayList<>();
		results.forEach(x -> findCombinationsForResult(x, pool, combinations));
		return combinations;
	}

	private void findCombinationsForResult(String result, WordPool pool, List<Combo> combinations) {
		Set<String> partials = pool.getPartialWords();
		for (String partial : partials) {
			Combo combo = new Combo(result);
			combo.addPartial(partial);
			if (combo.canContinue()) {
				addPartialsToCombination(combo, partials, combinations);
			}
		}
	}

	private void addPartialsToCombination(Combo combo, Set<String> partials, List<Combo> combinations) {
		for (String partial : partials) {
			combo.addPartial(partial);
			if (combo.canContinue()) {
				if (combo.isMatch()) {
					combinations.add(new Combo(combo));
				}
				addPartialsToCombination(combo, partials, combinations);
			}
			combo.removePartial();
		}
	}

}

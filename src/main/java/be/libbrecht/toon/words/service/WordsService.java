package be.libbrecht.toon.words.service;

import java.util.List;
import java.util.Set;

import be.libbrecht.toon.words.domain.Combo;
import be.libbrecht.toon.words.domain.WordPool;

public interface WordsService {
	WordPool createWordPool(Set<String> words, int resultWordLength);

	List<Combo> findCombinations(WordPool pool);
}

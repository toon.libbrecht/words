package be.libbrecht.toon.words.service;

import java.util.Set;

public interface InputService {
	Set<String> parseLines(String text);
}

package be.libbrecht.toon.words.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import be.libbrecht.toon.words.domain.Combo;
import be.libbrecht.toon.words.domain.WordPool;
import be.libbrecht.toon.words.service.InputService;
import be.libbrecht.toon.words.service.WordsService;

@RestController
public class FileController {

	@Autowired
	private WordsService service;

	@Autowired
	private InputService fileInputService;

	@Autowired
	private InputService stringInputService;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/api/file/test", method = RequestMethod.GET, produces = "text/plain")
	public String test(@RequestParam(defaultValue = "6") Integer wordLength) {
		WordPool wordPool = service.createWordPool(fileInputService.parseLines("input.txt"), wordLength);
		List<Combo> findCombinations = service.findCombinations(wordPool);
		return createResponse(findCombinations);
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/api/file", method = RequestMethod.POST, produces = "text/plain", consumes = "text/plain")
	public String wordsApi(@RequestBody String input, @RequestParam(defaultValue = "6") Integer wordLength) {
		WordPool wordPool = service.createWordPool(stringInputService.parseLines(input), wordLength);
		List<Combo> findCombinations = service.findCombinations(wordPool);
		return createResponse(findCombinations);
	}

	private String createResponse(List<Combo> findCombinations) {
		return findCombinations.stream().map(Combo::toString).collect(Collectors.joining(System.lineSeparator()));
	}
}

package be.libbrecht.toon.words.domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WordPool {
	private final Set<String> results;
	private final Set<String> partialWords;
	private final int resultWordLength;

	public WordPool(Set<String> results, Set<String> partialWords, int resultWordLength) {
		this.results = results;
		this.partialWords = cleanupPartials(partialWords);
		this.resultWordLength = resultWordLength;
	}

	private Set<String> cleanupPartials(Set<String> partialsToBeCleaned) {
		Set<String> result = new HashSet<>();
		for (String partialWord : partialsToBeCleaned) {
			if (results.parallelStream().anyMatch(x -> x.contains(partialWord))) {
				result.add(partialWord);
			}
		}
		return result;
	}

	public Set<String> getResults() {
		return Collections.unmodifiableSet(results);
	}

	public Set<String> getPartialWords() {
		return Collections.unmodifiableSet(partialWords);
	}

	public int getResultWordLength() {
		return resultWordLength;
	}

}

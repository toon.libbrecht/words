package be.libbrecht.toon.words.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Combo {
	private List<String> combination = new ArrayList<>();
	private String combinationString;

	private final String result;

	public Combo(String result) {
		this.result = result;
	}

	public Combo(Combo combo) {
		this.result = combo.result;
		this.combination = new ArrayList<String>(combo.combination);
		this.combinationString = combo.combinationString;
	}

	public void addPartial(String partial) {
		combination.add(partial);
		setCombinationString();
	}

	private void setCombinationString() {
		this.combinationString = combination.stream().collect(Collectors.joining());

	}

	public void removePartial() {
		combination.remove(combination.size() - 1);
		setCombinationString();
	}

	public boolean canContinue() {
		if (combinationString.length() > result.length()) {
			return false;
		}

		if (result.startsWith(combinationString)) {
			return true;
		}
		return false;
	}

	public boolean isMatch() {
		return result.equals(combinationString);
	}

	@Override
	public String toString() {
		return combination.stream().collect(Collectors.joining("+")) + "=" + result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((combinationString == null) ? 0 : combinationString.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Combo other = (Combo) obj;
		if (combinationString == null) {
			if (other.combinationString != null)
				return false;
		} else if (!combinationString.equals(other.combinationString))
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		return true;
	}

}

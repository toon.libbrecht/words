package be.libbrecht.toon.words.exception;

public class WordsException extends RuntimeException {

	private static final long serialVersionUID = -7029305372593585800L;

	public WordsException() {
	}

	public WordsException(Throwable e) {
		super(e);
	}

}

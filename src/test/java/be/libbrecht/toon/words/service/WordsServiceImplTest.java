package be.libbrecht.toon.words.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import be.libbrecht.toon.words.domain.Combo;
import be.libbrecht.toon.words.domain.WordPool;

public class WordsServiceImplTest {

	private WordsService wordsService = new WordsServiceImpl();

	@Test
	public void testWordPoolCreation() {
		Set<String> regularWordList = Set.of("zambia", "zmbia", "z", "zomb", "zombia", "a", "zas", "zam", "zom", "zama");
		WordPool createWordPool = wordsService.createWordPool(regularWordList, 6);
		assertNotNull(createWordPool);
		assertEquals(2, createWordPool.getResults().size());
		assertEquals(5, countValues(createWordPool));
	}

	@Test
	public void testWordPoolCreation_withDuplicates_filtered() {
		// Set.of can't have duplicates
		List<String> regularWordList = List.of("zambia", "zmbia", "z", "zom", "zambia", "a", "zas", "zom", "zom", "zama");
		WordPool createWordPool = wordsService.createWordPool(new HashSet<String>(regularWordList), 6);
		assertNotNull(createWordPool);
		assertEquals(1, createWordPool.getResults().size());
		assertEquals(2, countValues(createWordPool));
	}

	@Test
	public void testWordPoolCreation_with_too_long_words() {
		Set<String> regularWordList = Set.of("zambia", "zmbia", "z", "zomb", "zombias", "a", "zas", "zam", "zom", "zama");
		WordPool createWordPool = wordsService.createWordPool(regularWordList, 6);
		assertNotNull(createWordPool);
		assertEquals(1, createWordPool.getResults().size());
		assertEquals(3, countValues(createWordPool));
	}

	@Test
	public void testWordPoolCreation_with_empty_word_list() {
		Set<String> regularWordList = new HashSet<String>();
		WordPool createWordPool = wordsService.createWordPool(regularWordList, 6);
		assertNotNull(createWordPool);
		assertEquals(0, createWordPool.getResults().size());
		assertEquals(0, countValues(createWordPool));
	}

	@Test
	public void testWordPoolCreation_with_empty_word_should_be_empty() {
		Set<String> regularWordList = Set.of("");
		WordPool createWordPool = wordsService.createWordPool(regularWordList, 6);
		assertNotNull(createWordPool);
		assertEquals(0, createWordPool.getResults().size());
		assertEquals(0, countValues(createWordPool));
	}

	@Test
	public void testFindCombinations() {
		Set<String> results = Set.of("foobar");
		Set<String> partials = Set.of("foo", "bar", "r");
		Combo testCombo = new Combo("foobar");
		testCombo.addPartial("foo");
		testCombo.addPartial("bar");
		WordPool createWordPool = new WordPool(results, partials, 6);
		List<Combo> findCombinations = wordsService.findCombinations(createWordPool);
		assertEquals(1, findCombinations.size());
		Combo combination = findCombinations.get(0);
		assertEquals(testCombo, combination);
	}

	@Test
	public void testFindCombinations_with_multiple_combos() {
		Set<String> results = Set.of("foobar");
		Set<String> partials = Set.of("foo", "bar", "r", "f", "o", "b", "a");
		Combo testCombo1 = new Combo("foobar");
		testCombo1.addPartial("foo");
		testCombo1.addPartial("bar");
		Combo testCombo2 = new Combo("foobar");
		testCombo2.addPartial("f");
		testCombo2.addPartial("o");
		testCombo2.addPartial("o");
		testCombo2.addPartial("b");
		testCombo2.addPartial("a");
		testCombo2.addPartial("r");
		WordPool createWordPool = new WordPool(results, partials, 6);
		List<Combo> findCombinations = wordsService.findCombinations(createWordPool);
		assertEquals(4, findCombinations.size());
		assertTrue(findCombinations.contains(testCombo1));
		assertTrue(findCombinations.contains(testCombo2));
	}

	@Test
	public void testFindCombinations_without_result() {
		Set<String> results = Set.of("foobar");
		Set<String> partials = Set.of("foo", "baar", "r");
		WordPool createWordPool = new WordPool(results, partials, 6);
		List<Combo> findCombinations = wordsService.findCombinations(createWordPool);
		assertEquals(0, findCombinations.size());
	}

	@Test
	public void testFindCombinations_without_result_word() {
		Set<String> results = Set.of();
		Set<String> partials = Set.of("foo", "bar", "r");
		WordPool createWordPool = new WordPool(results, partials, 6);
		List<Combo> findCombinations = wordsService.findCombinations(createWordPool);
		assertEquals(0, findCombinations.size());
	}

	@Test
	public void testFindCombinations_without_partials() {
		Set<String> results = Set.of("foobar");
		Set<String> partials = Set.of();
		WordPool createWordPool = new WordPool(results, partials, 6);
		List<Combo> findCombinations = wordsService.findCombinations(createWordPool);
		assertEquals(0, findCombinations.size());
	}

	@Test
	public void testFindCombinations_with_length_2() {
		Set<String> results = Set.of("fo");
		Set<String> partials = Set.of("f", "o");
		Combo testCombo = new Combo("fo");
		testCombo.addPartial("f");
		testCombo.addPartial("o");
		WordPool createWordPool = new WordPool(results, partials, 2);
		List<Combo> findCombinations = wordsService.findCombinations(createWordPool);
		assertEquals(1, findCombinations.size());
		Combo combination = findCombinations.get(0);
		assertEquals(testCombo, combination);
	}

	private Integer countValues(WordPool createWordPool) {
		return createWordPool.getPartialWords().size();
	}
}

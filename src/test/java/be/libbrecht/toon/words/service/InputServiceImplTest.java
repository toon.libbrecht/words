package be.libbrecht.toon.words.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.Test;

public class InputServiceImplTest {

	private InputService fileInputService = new FileInputService();

	private InputService stringInputService = new StringInputService();

	@Test
	public void testParseInputFile() {
		Set<String> inputLines = fileInputService.parseLines("input.txt");
		assertNotNull(inputLines);
		assertFalse(inputLines.isEmpty());
		assertTrue(inputLines.contains("zambia"));
	}

	@Test
	public void testParseInputString() {
		Set<String> inputLines = stringInputService.parseLines("zambia\nzomzom");
		assertNotNull(inputLines);
		assertFalse(inputLines.isEmpty());
		assertTrue(inputLines.contains("zambia"));
		assertTrue(inputLines.contains("zomzom"));
	}
}
